package compiler.utils.bytecode.bytecodeGenerators;

import antlr.nixlangParser;
import compiler.utils.bytecode.Instruction;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class LiteralGenerator implements Instruction, Opcodes {
    private nixlangParser.LiteralContext ctx;

    public LiteralGenerator(nixlangParser.LiteralContext ctx){
        this.ctx = ctx;
    }

    @Override
    public void apply(MethodVisitor mv) {
        if(ctx.NONE() != null){
            mv.visitInsn(ACONST_NULL);
        }
    }
}
