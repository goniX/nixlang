package compiler.utils.bytecode.bytecodeGenerators;

import antlr.nixlangParser;
import compiler.utils.bytecode.Instruction;
import compiler.utils.bytecode.Type;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.MethodVisitor;

import static compiler.utils.bytecode.bytecodeGenerators.TypeCast.*;

public class ExpressionGenerator implements Instruction, Opcodes {
    private nixlangParser.ExpressionContext ctx;

    public ExpressionGenerator(nixlangParser.ExpressionContext ctx){
        this.ctx = ctx;
    }

    @Override
    public void apply(MethodVisitor mv) {
        if (this.ctx.binaryOp() != null) {
            int typeLeft = getExpressionType(this.ctx.expression(0));
            int typeRight = getExpressionType(this.ctx.expression(1));

            if ((typeLeft != typeRight) && (typeLeft != -1)) {
                typeCast(mv, typeRight, typeLeft);
            }

            String op = this.ctx.binaryOp().getText();
            if (typeLeft == Type.INTEGER.getValue()) {
                switch (op) {
                    case ("+"):
                        mv.visitInsn(IADD);
                        break;
                    case ("-"):
                        mv.visitInsn(ISUB);
                        break;
                    case ("*"):
                        mv.visitInsn(IMUL);
                        break;
                    case ("/"):
                        mv.visitInsn(IDIV);
                        break;
                    case ("%"):
                        mv.visitInsn(IREM);
                        break;
                }
            } else if (typeLeft == Type.DOUBLE.getValue()) {
                switch (op) {
                    case ("+"):
                        mv.visitInsn(DADD);
                        break;
                    case ("-"):
                        mv.visitInsn(DSUB);
                        break;
                    case ("*"):
                        mv.visitInsn(DMUL);
                        break;
                    case ("/"):
                        mv.visitInsn(DDIV);
                        break;
                    case ("%"):
                        mv.visitInsn(DREM);
                        break;
                }
            }
        }
    }
}
