package compiler.utils.bytecode.bytecodeGenerators;

import antlr.nixlangParser;
import compiler.utils.bytecode.Instruction;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.MethodVisitor;

public class StringGenerator implements Instruction, Opcodes {
    private nixlangParser.StringContext ctx;

    public StringGenerator(nixlangParser.StringContext ctx){
        this.ctx = ctx;
    }

    @Override
    public void apply(MethodVisitor mv) {
        String str = ctx.getText();
        System.out.println(str);
        str = str.substring(1, str.length()-1);
        mv.visitLdcInsn(str);
    }
}